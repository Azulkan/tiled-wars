<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Ground" tilewidth="512" tileheight="512" tilecount="19" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="512" height="512" source="tiles/dirt.png"/>
 </tile>
 <tile id="1">
  <image width="512" height="512" source="tiles/grass1.png"/>
 </tile>
 <tile id="2">
  <image width="512" height="512" source="tiles/grass2.png"/>
 </tile>
 <tile id="3">
  <image width="512" height="512" source="tiles/grass3.png"/>
 </tile>
 <tile id="4">
  <image width="512" height="512" source="tiles/rock.png"/>
 </tile>
 <tile id="5">
  <image width="512" height="512" source="tiles/rock2.png"/>
 </tile>
 <tile id="6">
  <image width="512" height="512" source="tiles/rock3.png"/>
 </tile>
 <tile id="7">
  <image width="512" height="512" source="tiles/rock4.png"/>
 </tile>
 <tile id="8">
  <image width="512" height="512" source="tiles/water1.png"/>
 </tile>
 <tile id="9">
  <image width="512" height="512" source="tiles/water2.png"/>
 </tile>
 <tile id="10">
  <image width="512" height="512" source="tiles/water3.png"/>
 </tile>
 <tile id="11">
  <image width="512" height="512" source="tiles/farm.png"/>
 </tile>
 <tile id="13">
  <image width="512" height="512" source="tiles/trees.png"/>
 </tile>
 <tile id="19">
  <image width="512" height="512" source="tiles/tower.png"/>
 </tile>
 <tile id="20">
  <image width="512" height="512" source="tiles/tent.png"/>
 </tile>
 <tile id="21">
  <image width="512" height="512" source="tiles/tent2.png"/>
 </tile>
 <tile id="22">
  <image width="512" height="512" source="tiles/castle2.png"/>
 </tile>
 <tile id="23">
  <image width="512" height="512" source="tiles/castle1.png"/>
 </tile>
 <tile id="24">
  <image width="512" height="512" source="tiles/workshop.png"/>
 </tile>
</tileset>
