<?xml version="1.0" encoding="UTF-8"?>
<tileset name="overlay" tilewidth="512" tileheight="512" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="512" height="512" source="white.png"/>
 </tile>
 <tile id="1">
  <image width="512" height="512" source="red.png"/>
 </tile>
</tileset>
