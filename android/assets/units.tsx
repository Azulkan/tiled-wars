<?xml version="1.0" encoding="UTF-8"?>
<tileset name="units" tilewidth="512" tileheight="512" tilecount="8" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="12">
  <image width="512" height="512" source="units/sword.png"/>
 </tile>
 <tile id="13">
  <image width="512" height="512" source="units/mace.png"/>
 </tile>
 <tile id="14">
  <image width="512" height="512" source="units/horse.png"/>
 </tile>
 <tile id="15">
  <image width="512" height="512" source="units/hand.png"/>
 </tile>
 <tile id="16">
  <image width="512" height="512" source="units/crown.png"/>
 </tile>
 <tile id="17">
  <image width="512" height="512" source="units/catapult.png"/>
 </tile>
 <tile id="18">
  <image width="512" height="512" source="units/cannon.png"/>
 </tile>
 <tile id="19">
  <image width="512" height="512" source="units/bow.png"/>
 </tile>
</tileset>
