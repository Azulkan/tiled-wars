package com.azulkan.tiledwars;

import com.azulkan.tiledwars.model.Game;
import com.azulkan.tiledwars.view.Hud;
import com.azulkan.tiledwars.controller.TiledMapStage;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class TiledWars extends ApplicationAdapter implements GestureDetector.GestureListener {
	private static final int tileSize = 512;
	private static final int WORLD_WIDTH = 30 * tileSize;
	private static final int WORLD_HEIGHT = 30 * tileSize;
	private float currentZoom;

	private Game game;

	private SpriteBatch batch;

	private TiledMap map;
	private OrthographicCamera cam;
	private OrthogonalTiledMapRenderer mapRenderer;

	private Hud hud;

	@Override
	public void create () {
		batch = new SpriteBatch();

		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		/* Camera */
		cam = new OrthographicCamera(WORLD_WIDTH / 2, WORLD_HEIGHT / 2 * (h / w));
		cam.position.set(cam.viewportWidth / 2f, cam.viewportHeight / 2f, 0);
		cam.update();
		currentZoom = cam.zoom;

		/* Initialize the map */
		map = new TmxMapLoader().load("map.tmx");
		mapRenderer = new OrthogonalTiledMapRenderer(map);

		game = new Game(2, map);
		hud = new Hud(batch, game);

		/* Multiplexed input processors */
		InputProcessor gestureInputProcessor = new GestureDetector(this);

		Stage mapStage = new TiledMapStage(map, hud);
		mapStage.getViewport().setCamera(cam);

		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(gestureInputProcessor);
		inputMultiplexer.addProcessor(hud.stage);
		inputMultiplexer.addProcessor(mapStage);
		Gdx.input.setInputProcessor(inputMultiplexer);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		game.update();

		mapRenderer.setView(cam);
		mapRenderer.render();

		batch.setProjectionMatrix(hud.stage.getCamera().combined);
		hud.stage.draw();
		hud.update();
		batch.begin();
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		hud.dispose();
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		cam.translate(-deltaX * 4 * currentZoom, deltaY * 4 * currentZoom);
		afterInput();
		cam.update();
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		currentZoom = cam.zoom;
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		float ratio = initialDistance / distance;
		cam.zoom = ratio * currentZoom;
		afterInput();
		cam.update();
		return true;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public void pinchStop() {

	}

	private void afterInput() {
		cam.zoom = MathUtils.clamp(cam.zoom, 0.5f, WORLD_WIDTH/cam.viewportWidth);

		float effectiveViewportWidth = cam.viewportWidth * cam.zoom;
		float effectiveViewportHeight = cam.viewportHeight * cam.zoom;

		cam.position.x = MathUtils.clamp(cam.position.x, effectiveViewportWidth / 2f, WORLD_WIDTH - effectiveViewportWidth / 2f);
		cam.position.y = MathUtils.clamp(cam.position.y, effectiveViewportHeight / 2f, WORLD_HEIGHT - effectiveViewportHeight / 2f);
	}
}
