package com.azulkan.tiledwars.controller;

/**
 * Created by pauls on 02/12/17.
 */

public class MissingUnitException extends Exception {
    public MissingUnitException(String message) {
        super(message);
    }
}
