package com.azulkan.tiledwars.controller;

import com.azulkan.tiledwars.model.Entity;
import com.azulkan.tiledwars.model.buildings.Building;
import com.azulkan.tiledwars.model.units.King;
import com.azulkan.tiledwars.model.units.Maceman;
import com.azulkan.tiledwars.model.units.Swordman;
import com.azulkan.tiledwars.model.units.Unit;
import com.azulkan.tiledwars.model.units.Worker;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.ArrayList;

/**
 * Created by pauls on 28/11/17.
 */

public class TiledMapActor extends Actor {
    private TiledMap map;
    private TiledMapTileLayer tiledLayer;
    private TiledMapTileLayer.Cell cell;
    private int x, y;
    private ArrayList<Entity> entities;

    public TiledMapActor(TiledMap tiledMap, TiledMapTileLayer tiledLayer, TiledMapTileLayer.Cell cell, int x, int y) {
        this.map = tiledMap;
        this.tiledLayer = tiledLayer;
        this.cell = cell;
        this.x = x;
        this.y = y;
        this.entities = new ArrayList<Entity>();
    }

    public TiledMapTileLayer.Cell getCell() {
        return this.cell;
    }

    public int distanceTo(TiledMapActor a) {
        return(Math.abs(this.x - a.getTileX()) + Math.abs(this.y - a.getTileY()));
    }

    public int distanceTo(int x, int y) {
        return(Math.abs(this.x - x) + Math.abs(this.y - y));
    }

    public int getTileX() {
        return x;
    }

    public int getTileY() {
        return y;
    }

    public ArrayList<Entity> getEntities() {
        return this.entities;
    }

    public Worker selectWorker() throws MissingUnitException {
        Worker res = null;
        for (Entity e : entities) {
            if (e instanceof Worker) {
                res = (Worker)e;
            }
        }
        if(res == null) {
            throw new MissingUnitException("No worker to be selected.");
        }
        return res;
    }

    public King selectKing() throws MissingUnitException {
        King res = null;
        for (Entity e : entities) {
            if (e instanceof King) {
                res = (King)e;
            }
        }
        if(res == null) {
            throw new MissingUnitException("No worker to be selected.");
        }
        return res;
    }

    public Swordman selectSwordman() throws MissingUnitException {
        Swordman res = null;
        for (Entity e : entities) {
            if (e instanceof Swordman) {
                res = (Swordman)e;
            }
        }
        if(res == null) {
            throw new MissingUnitException("No swordman to be selected.");
        }
        return res;
    }

    public Maceman selectMaceman() throws MissingUnitException {
        Maceman res = null;
        for (Entity e : entities) {
            if (e instanceof Maceman) {
                res = (Maceman)e;
            }
        }
        if(res == null) {
            throw new MissingUnitException("No maceman to be selected.");
        }
        return res;
    }

    public Unit getUnit() {
        for(Entity e : getEntities()) {
            if(e instanceof Unit) {
                return (Unit)e;
            }
        }
        return null;
    }

    public boolean hasBuilding() {
        for(Entity e : getEntities()) {
            if(e instanceof Building) {
                return true;
            }
        }
        return false;
    }

    public boolean hasUnit() {
        for(Entity e : getEntities()) {
            if(e instanceof Unit) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("Tile ").append(this.getCell()).append(" has been clicked.\n");
        res.append("ID: ").append(this.getCell().getTile().getId()).append('\n');
        res.append("Position: x=").append(x).append(";y=").append(y).append('\n');
        res.append("Entities: ");
        for(Entity e : entities) {
            res.append(e.toString());
            res.append(", ");
        }
        return res.toString();
    }
}
