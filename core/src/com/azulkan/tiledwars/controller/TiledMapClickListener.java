package com.azulkan.tiledwars.controller;

import com.azulkan.tiledwars.model.MissingResourceException;
import com.azulkan.tiledwars.model.units.MoveUnitException;
import com.azulkan.tiledwars.model.units.Unit;
import com.azulkan.tiledwars.model.units.Worker;
import com.azulkan.tiledwars.view.ActionType;
import com.azulkan.tiledwars.view.Hud;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by pauls on 28/11/17.
 */

public class TiledMapClickListener extends ClickListener {
    private TiledMapActor actor;
    private Hud hud;

    public TiledMapClickListener(TiledMapActor actor, Hud hud) {
        this.actor = actor;
        this.hud = hud;
    }

    @Override
    /* This method is called each time a tile is clicked. Behaviour depends on the HUD's ActionType */
    public void clicked(InputEvent event, float x, float y) {
        System.out.println(actor.toString()); // Print tile info

        switch(hud.getActionType()) { // switch over HUD's ActionType to find out what's to do
            case SELECT_TILE:
                if(!actor.hasBuilding()) { // If there is no building on the tile
                    if(actor.hasUnit()) { // If there is a unit on the tile
                        if(actor.getUnit() instanceof Worker) {
                            hud.clear();
                            System.out.println("Worker selected.");
                            hud.setSelection(actor.getUnit()); // Set the unit as HUD's selection
                            hud.makeWorkerTable(actor);
                            break;
                        }
                    }
                } else { // If there is a building on the tile, open corresponding menu
                    int tileId = actor.getCell().getTile().getId();
                    switch (tileId) {
                        case 23: case 24: // If a castle has been clicked
                            if(hud.getGame().getRound().getPlayer().getId() == 1 && tileId == 24 ||
                                    hud.getGame().getRound().getPlayer().getId() == 2 && tileId == 23) {
                                hud.makeCastleTable(actor); // Make menu for castle
                            } break;

                        case 20: // If a barrack has been clicked
                            hud.makeBarracksTable(actor); // Make menu for castle
                            break;
                        default: break;
                    }
                } break;

            case MENU:
                /* Click out of the menu -> cancel */
                hud.clear();
                hud.setActionType(ActionType.SELECT_TILE);
                break;

            case MOVE_UNIT:
                /* Check if the movement is allowed, otherwise raise exception */
                try {
                    ((Unit)hud.getSelection()).moveTo(actor);
                    hud.setActionType(ActionType.SELECT_TILE);
                } catch(MoveUnitException e) {
                    e.printStackTrace();
                }
                hud.clear();
                hud.setSelection(null);
                break;

            case BUILD_FARM:
                try {
                    TiledMapTileLayer groundLayer = (TiledMapTileLayer)hud.getGame().getMap().getLayers().get("Ground");
                    TiledMapTile tile = hud.getGame().getMap().getTileSets().getTileSet("Ground").getTile(12);
                    ((Worker)hud.getSelection()).buildFarm(actor);
                    actor.getCell().setTile(tile);
                } catch(MissingResourceException e) {
                    e.printStackTrace();
                } catch(MoveUnitException e) {
                    e.printStackTrace();
                }
                hud.setActionType(ActionType.SELECT_TILE);
                hud.clear();
                hud.setSelection(null);
                break;

            case BUILD_BARRACKS:
                try {
                    TiledMapTileLayer groundLayer = (TiledMapTileLayer)hud.getGame().getMap().getLayers().get("Ground");
                    TiledMapTile tile = hud.getGame().getMap().getTileSets().getTileSet("Ground").getTile(21);
                    ((Worker) hud.getSelection()).buildBarracks(actor);
                    actor.getCell().setTile(tile);
                } catch(MissingResourceException e) {
                    e.printStackTrace();
                }
                hud.setActionType(ActionType.SELECT_TILE);
                hud.clear();
                hud.setSelection(null);
                break;

            case BUILD_ARCHERY:
                try {
                    TiledMapTileLayer groundLayer = (TiledMapTileLayer)hud.getGame().getMap().getLayers().get("Ground");
                    TiledMapTile tile = hud.getGame().getMap().getTileSets().getTileSet("Ground").getTile(22);
                    ((Worker) hud.getSelection()).buildArchery(actor);
                    actor.getCell().setTile(tile);
                } catch(MissingResourceException e) {
                    e.printStackTrace();
                }
                hud.setActionType(ActionType.SELECT_TILE);
                hud.clear();
                hud.setSelection(null);
                break;

            case BUILD_WORKSHOP:
                try {
                    TiledMapTileLayer groundLayer = (TiledMapTileLayer)hud.getGame().getMap().getLayers().get("Ground");
                    TiledMapTile tile = hud.getGame().getMap().getTileSets().getTileSet("Ground").getTile(25);
                    ((Worker) hud.getSelection()).buildWorkshop(actor);
                    actor.getCell().setTile(tile);
                } catch(MissingResourceException e) {
                    e.printStackTrace();
                }
                hud.setActionType(ActionType.SELECT_TILE);
                hud.clear();
                hud.setSelection(null);
                break;

            case BUILD_TOWER:
                try {
                    TiledMapTileLayer groundLayer = (TiledMapTileLayer)hud.getGame().getMap().getLayers().get("Ground");
                    TiledMapTile tile = hud.getGame().getMap().getTileSets().getTileSet("Ground").getTile(20);
                    ((Worker) hud.getSelection()).buildTower(actor);
                    actor.getCell().setTile(tile);
                } catch(MissingResourceException e) {
                    e.printStackTrace();
                }
                hud.setActionType(ActionType.SELECT_TILE);
                hud.clear();
                hud.setSelection(null);
                break;

            default: break;
        }

    }
}
