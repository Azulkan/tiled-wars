package com.azulkan.tiledwars.controller;

import com.azulkan.tiledwars.model.Player;
import com.azulkan.tiledwars.model.buildings.Castle;
import com.azulkan.tiledwars.model.units.King;
import com.azulkan.tiledwars.view.Hud;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by pauls on 28/11/17.
 */

public class TiledMapStage extends Stage {
    private TiledMap tiledMap;
    private Hud hud;

    public TiledMapStage(TiledMap tiledMap, Hud hud) {
        this.tiledMap = tiledMap;
        this.hud = hud;

        MapLayer layer = tiledMap.getLayers().get("ground");
        createActorsForLayer((TiledMapTileLayer)layer);
        /*
        for (MapLayer layer : tiledMap.getLayers()) {
            TiledMapTileLayer tiledLayer = (TiledMapTileLayer)layer;
            createActorsForLayer(tiledLayer);
        }
        */
    }

    private void createActorsForLayer(TiledMapTileLayer tiledLayer) {
        for(int x = 0; x < tiledLayer.getWidth(); x++) {
            for(int y = 0; y < tiledLayer.getHeight(); y++) {
                TiledMapTileLayer.Cell cell = tiledLayer.getCell(x, y); // Where are the cells created ?
                TiledMapActor actor = new TiledMapActor(tiledMap, tiledLayer, cell, x, y);
                actor.setBounds(x * tiledLayer.getTileWidth(), y * tiledLayer.getTileHeight(), tiledLayer.getTileWidth(), tiledLayer.getTileHeight());
                addActor(actor);
                EventListener eventListener = new TiledMapClickListener(actor, hud);
                actor.addListener(eventListener);

                /* If the cell is a castle, assign it to a player */

                if(tiledLayer.getCell(x, y).getTile().getId() == 24 ||
                        tiledLayer.getCell(x, y).getTile().getId() == 23) {
                    Player p = null;
                    Castle c = null;
                    King k = null;
                    if(tiledLayer.getCell(x, y).getTile().getId() == 24) {
                        p = hud.getGame().getPlayers().get(0);
                    } else {
                        p = hud.getGame().getPlayers().get(1);
                    }
                    c = new Castle(actor, p);
                    k = new King(actor, p);
                    p.getBuildings().add(c);
                    p.getArmy().add(k);
                    actor.getEntities().add(c);
                    actor.getEntities().add(k);
                }
            }
        }
    }
}
