package com.azulkan.tiledwars.model;

/**
 * Created by pauls on 30/11/17.
 */

public final class Constants {
    public static final int ROUND_DURATION_MILLIS = 60000;

    public static final int KING_MOVE_POINTS = 2;

    public static final int WORKER_HEALTH_POINTS = 2;
    public static final int WORKER_MOVE_POINTS = 4;
    public static final int WORKER_DEFENCE_POINTS = 0;
    public static final int WORKER_ATTACK_POINTS = 0;
    public static final int WORKER_COST_WHEAT = 50;

    public static final int SWORDMAN_MOVE_POINTS = 3;
    public static final int SWORDMAN_COST_WHEAT = 50;
    public static final int SWORDMAN_COST_WOOD = 50;

    public static final int MACEMAN_MOVE_POINTS = 2;
    public static final int MACEMAN_COST_WHEAT = 50;
    public static final int MACEMAN_COST_WOOD = 50;
    public static final int MACEMAN_COST_ROCK = 50;

    public static final int BARRACKS_COST_ROCK = 10;
    public static final int BARRACKS_COST_WOOD = 10;

    public static final int ARCHERY_COST_ROCK = 10;
    public static final int ARCHERY_COST_WOOD = 10;

    public static final int WORKSHOP_COST_ROCK = 10;
    public static final int WORKSHOP_COST_WOOD = 10;

    public static final int TOWER_COST_ROCK = 10;
    public static final int TOWER_COST_WOOD = 10;

    public static final int FARM_COST_WHEAT = 10;
    public static final int FARM_COST_WOOD = 10;

    public static final int FARM_PRODUCTIVITY = 50;
    public static final int ROCK_PRODUCTIVITY = 50;
    public static final int WOOD_PRODUCTIVITY = 50;
    public static final int WHEAT_START = 500;
    public static final int ROCK_START = 500;
    public static final int WOOD_START = 500;
}
