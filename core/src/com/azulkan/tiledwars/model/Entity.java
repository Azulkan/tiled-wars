package com.azulkan.tiledwars.model;

import com.azulkan.tiledwars.controller.TiledMapActor;

/**
 * Created by pauls on 01/12/17.
 */

public abstract class Entity {
    private TiledMapActor actor;
    private Player owner;

    public Entity(TiledMapActor actor, Player owner) {
        setTiledActor(actor);
        setOwner(owner);
    }

    public TiledMapActor getTiledActor() {
        return this.actor;
    }

    public void setTiledActor(TiledMapActor actor) {
        this.actor = actor;
    }

    public Player getOwner() {
        return this.owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("Entity");
        return res.toString();
    }
}
