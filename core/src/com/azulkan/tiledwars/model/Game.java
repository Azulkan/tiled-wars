package com.azulkan.tiledwars.model;

import com.azulkan.tiledwars.model.units.King;
import com.azulkan.tiledwars.model.units.Maceman;
import com.azulkan.tiledwars.model.units.Swordman;
import com.azulkan.tiledwars.model.units.Unit;
import com.azulkan.tiledwars.model.units.Worker;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import java.util.ArrayList;

/**
 * Created by Paul on 29/11/2017.
 */

public class Game {
    private ArrayList<Player> players;
    private Round round;
    private TiledMap map;

    public Game(int nbPlayers, TiledMap map) {
        Player.idMax = 0;
        players = new ArrayList<Player>();
        for(int i = 0; i < nbPlayers; i++) {
            players.add(new Player());
        }

        /* Assign castles to players */
        TiledMapTileLayer groundLayer = (TiledMapTileLayer) map.getLayers().get("ground");

        round = new Round(players.get(0));
        this.map = map;
    }

    public void update() {
        /* Check round time */
        if(round.getTime() < Constants.ROUND_DURATION_MILLIS) {
            round.update();
        } else {
            round = new Round(getNextPlayer(round.getPlayer()));
        }

        /* Update units sprites */
        TiledMapTileLayer unitsLayer = (TiledMapTileLayer) map.getLayers().get("units");

        /* Clear all cells */
        for(int x = 0; x < unitsLayer.getWidth(); x++) {
            for(int y = 0; y < unitsLayer.getHeight(); y++) {
                unitsLayer.setCell(x, y, null);
            }
        }

        TiledMapTile tile = null;
        for(Unit u : round.getPlayer().getArmy()) {
            int x = u.getTiledActor().getTileX();
            int y = u.getTiledActor().getTileY();

            if(!u.getTiledActor().hasBuilding()) { // Don't show unit if it is in a building
                if(u instanceof Worker) {
                    tile = map.getTileSets().getTileSet("units").getTile(41);
                    unitsLayer.setCell(x, y, new TiledMapTileLayer.Cell());
                    unitsLayer.getCell(x, y).setTile(tile);
                } else if(u instanceof King) {
                    tile = map.getTileSets().getTileSet("units").getTile(42);
                    unitsLayer.setCell(x, y, new TiledMapTileLayer.Cell());
                    unitsLayer.getCell(x, y).setTile(tile);
                } else if(u instanceof Swordman) {
                    tile = map.getTileSets().getTileSet("units").getTile(38);
                    unitsLayer.setCell(x, y, new TiledMapTileLayer.Cell());
                    unitsLayer.getCell(x, y).setTile(tile);
                } else if(u instanceof Maceman) {
                    tile = map.getTileSets().getTileSet("units").getTile(39);
                    unitsLayer.setCell(x, y, new TiledMapTileLayer.Cell());
                    unitsLayer.getCell(x, y).setTile(tile);
                }
            }
        }



    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public Round getRound() {
        return round;
    }

    public Player getNextPlayer(Player player) {
        if(players.indexOf(player) == players.size() - 1) {
            return players.get(0);
        } else {
            return players.get(players.indexOf(player) + 1);
        }
    }

    public TiledMap getMap() {
        return map;
    }
}
