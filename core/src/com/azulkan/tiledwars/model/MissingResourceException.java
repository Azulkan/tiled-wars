package com.azulkan.tiledwars.model;

/**
 * Created by pauls on 04/12/17.
 */

public class MissingResourceException extends Exception {
    public MissingResourceException(String message) {
        super(message);
    }
}
