package com.azulkan.tiledwars.model;

import com.azulkan.tiledwars.model.buildings.Building;
import com.azulkan.tiledwars.model.buildings.Castle;
import com.azulkan.tiledwars.model.units.King;
import com.azulkan.tiledwars.model.units.Maceman;
import com.azulkan.tiledwars.model.units.Swordman;
import com.azulkan.tiledwars.model.units.Unit;
import com.azulkan.tiledwars.model.units.Worker;
import com.azulkan.tiledwars.controller.TiledMapActor;

import java.util.ArrayList;

/**
 * Created by Paul on 29/11/2017.
 */

public class Player {
    static int idMax;
    private int id;
    private int wheat, wood, rock;
    private ArrayList<Unit> army;
    private ArrayList<Building> buildings;

    public Player() {
        this.id = ++idMax;
        this.army = new ArrayList<Unit>();
        this.buildings = new ArrayList<Building>();
        this.wheat = Constants.WHEAT_START;
        this.wood = Constants.WOOD_START;
        this.rock = Constants.ROCK_START;
    }

    public void createWorker(TiledMapActor actor) throws MissingResourceException {
        if(wheat >= Constants.WORKER_COST_WHEAT) {
            Worker w = new Worker(actor, this);
            army.add(w);
            actor.getEntities().add(w);
            wheat -= Constants.WORKER_COST_WHEAT;
        } else {
            throw new MissingResourceException("Not enough resources to create a worker.");
        }
    }

    public void createSwordman(TiledMapActor actor) throws MissingResourceException {
        if(wheat >= Constants.SWORDMAN_COST_WHEAT &&
                wood >= Constants.SWORDMAN_COST_WOOD) {
            Swordman s = new Swordman(actor, this);
            army.add(s);
            actor.getEntities().add(s);
            wheat -= Constants.SWORDMAN_COST_WHEAT;
        } else {
            throw new MissingResourceException("Not enough resources to create a swordman.");
        }
    }

    public void createMaceman(TiledMapActor actor) throws MissingResourceException {
        if(wheat >= Constants.MACEMAN_COST_WHEAT &&
                rock >= Constants.MACEMAN_COST_ROCK &&
                wood >= Constants.MACEMAN_COST_WOOD) {
            Maceman m = new Maceman(actor, this);
            army.add(m);
            actor.getEntities().add(m);
            wheat -= Constants.MACEMAN_COST_WHEAT;
        } else {
            throw new MissingResourceException("Not enough resources to create a maceman.");
        }
    }

    public void createCastle(TiledMapActor actor) {
        Castle c = new Castle(actor, this);
        buildings.add(c);
        actor.getEntities().add(c);
    }

    public void harvest() {
        for(Worker w : getWorkers()) {
            switch(w.getCurrentLabor()) {
                case FARM: wheat += Constants.FARM_PRODUCTIVITY; break;
                case ROCK: rock += Constants.ROCK_PRODUCTIVITY; break;
                case WOOD: wood += Constants.WOOD_PRODUCTIVITY; break;
                default: break;
            }
        }
    }

    public ArrayList<Unit> getArmy() {
        return army;
    }

    public ArrayList<Worker> getWorkers() {
        ArrayList<Worker> res = new ArrayList<Worker>();
        for(Unit u : getArmy()) {
            if(u instanceof Worker) {
                res.add((Worker)u);
            }
        }
        return res;
    }

    public ArrayList<Building> getBuildings() {
        return this.buildings;
    }

    public int getId() {
        return id;
    }

    public int getWheat() {
        return wheat;
    }

    public int getWood() {
        return wood;
    }

    public int getRock() {
        return rock;
    }

    public void spend(String resource, int amount) {
        switch(resource) {
            case "wheat": wheat -= amount; break;
            case "wood": wood -= amount; break;
            case "rock": rock -= amount; break;
        }
    }

    public int getWorkersCount() {
        int res = 0;
        for(Unit u : army) {
            if(u instanceof Worker) {
                res++;
            }
        }
        return res;
    }

    public int getWorkersCount(TiledMapActor actor) {
        int res = 0;
        for(Unit u : army) {
            if(u instanceof Worker && u.getTiledActor() == actor) {
                res++;
            }
        }
        return res;
    }

    public int getKingsCount() {
        int res = 0;
        for(Unit u : army) {
            if(u instanceof King) {
                res++;
            }
        }
        return res;
    }

    public int getKingsCount(TiledMapActor actor) {
        int res = 0;
        for(Unit u : army) {
            if(u instanceof King && u.getTiledActor() == actor) {
                res++;
            }
        }
        return res;
    }

    public int getSwordmenCount() {
        int res = 0;
        for(Unit u : army) {
            if(u instanceof Swordman) {
                res++;
            }
        }
        return res;
    }

    public int getSwordmenCount(TiledMapActor actor) {
        int res = 0;
        for(Unit u : army) {
            if(u instanceof Swordman && u.getTiledActor() == actor) {
                res++;
            }
        }
        return res;
    }

    public int getMacemenCount() {
        int res = 0;
        for(Unit u : army) {
            if(u instanceof Maceman) {
                res++;
            }
        }
        return res;
    }

    public int getMacemenCount(TiledMapActor actor) {
        int res = 0;
        for(Unit u : army) {
            if(u instanceof Maceman && u.getTiledActor() == actor) {
                res++;
            }
        }
        return res;
    }
}
