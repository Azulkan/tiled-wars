package com.azulkan.tiledwars.model;

import com.azulkan.tiledwars.model.units.Worker;

/**
 * Created by pauls on 30/11/17.
 */

public class Round {
    private long startTime;
    private String strTime;
    private Player player;

    public Round(Player player) {
        this.player = player;
        player.harvest();
        for(Worker w : player.getWorkers()) {
            w.resetMp();
        }
        startTime = System.currentTimeMillis();
        strTime = "";
    }

    public void update() {
        long elapsed = (System.currentTimeMillis() - startTime);
        long seconds = (Constants.ROUND_DURATION_MILLIS - elapsed) / 1000;
        strTime = String.format("%02d:%02d", (seconds % 3600) / 60, (seconds % 60));
    }

    public long getTime() {
        return System.currentTimeMillis() - startTime;
    }
    public String getStrTime() {
        return strTime;
    }
    public Player getPlayer() {
        return player;
    }
}
