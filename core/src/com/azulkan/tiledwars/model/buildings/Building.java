package com.azulkan.tiledwars.model.buildings;


import com.azulkan.tiledwars.model.Player;
import com.azulkan.tiledwars.model.Entity;
import com.azulkan.tiledwars.controller.TiledMapActor;

/**
 * Created by pauls on 30/11/17.
 */

public abstract class Building extends Entity {
    public Building(TiledMapActor actor, Player owner) {
        super(actor, owner);
    }
}
