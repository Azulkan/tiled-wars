package com.azulkan.tiledwars.model.buildings;

import com.azulkan.tiledwars.model.Player;
import com.azulkan.tiledwars.controller.TiledMapActor;

/**
 * Created by pauls on 30/11/17.
 */

public class Castle extends Building {
    public Castle(TiledMapActor actor, Player owner) {
        super(actor, owner);
    }
}
