package com.azulkan.tiledwars.model.buildings;

import com.azulkan.tiledwars.controller.TiledMapActor;
import com.azulkan.tiledwars.model.Player;

/**
 * Created by Paul on 05/12/2017.
 */

public class Workshop extends Building {
    public Workshop(TiledMapActor actor, Player owner) {
        super(actor, owner);
    }
}
