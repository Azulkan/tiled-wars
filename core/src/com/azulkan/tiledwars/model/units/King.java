package com.azulkan.tiledwars.model.units;

import com.azulkan.tiledwars.model.Constants;
import com.azulkan.tiledwars.model.Player;
import com.azulkan.tiledwars.controller.TiledMapActor;

/**
 * Created by Paul on 04/12/2017.
 */

public class King extends Unit {
    public King(TiledMapActor actor, Player owner) {
        super(actor, owner);
        this.mp = Constants.KING_MOVE_POINTS;
    }

    @Override
    public void resetMp() {
        this.mp = Constants.KING_MOVE_POINTS;
    }
}
