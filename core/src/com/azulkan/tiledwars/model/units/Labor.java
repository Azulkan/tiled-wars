package com.azulkan.tiledwars.model.units;

/**
 * Created by pauls on 02/12/17.
 */

public enum Labor {
    FARM,
    ROCK,
    WOOD,
    NONE
}
