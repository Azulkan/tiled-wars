package com.azulkan.tiledwars.model.units;

import com.azulkan.tiledwars.model.Constants;
import com.azulkan.tiledwars.model.Player;
import com.azulkan.tiledwars.controller.TiledMapActor;

/**
 * Created by Paul on 04/12/2017.
 */

public class Maceman extends Unit {
    public Maceman(TiledMapActor actor, Player owner) {
        super(actor, owner);
        mp = Constants.MACEMAN_MOVE_POINTS;
    }

    @Override
    public void resetMp() {
        mp = Constants.MACEMAN_MOVE_POINTS;
    }
}
