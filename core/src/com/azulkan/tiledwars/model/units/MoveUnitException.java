package com.azulkan.tiledwars.model.units;

/**
 * Created by pauls on 03/12/17.
 */

public class MoveUnitException extends Exception {
    public MoveUnitException(String message) {
        super(message);
    }
}
