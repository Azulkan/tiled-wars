package com.azulkan.tiledwars.model.units;

import com.azulkan.tiledwars.model.Constants;
import com.azulkan.tiledwars.model.Player;
import com.azulkan.tiledwars.controller.TiledMapActor;

/**
 * Created by pauls on 29/11/17.
 */

public class Swordman extends Unit {
    public Swordman(TiledMapActor actor, Player owner) {
        super(actor, owner);
        mp = Constants.SWORDMAN_MOVE_POINTS;
    }

    @Override
    public void resetMp() {
        mp = Constants.SWORDMAN_MOVE_POINTS;
    }
}
