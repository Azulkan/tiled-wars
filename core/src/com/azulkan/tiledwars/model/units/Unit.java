package com.azulkan.tiledwars.model.units;

import com.azulkan.tiledwars.model.Player;
import com.azulkan.tiledwars.model.Entity;
import com.azulkan.tiledwars.view.Hud;
import com.azulkan.tiledwars.controller.TiledMapActor;

/**
 * Created by pauls on 29/11/17.
 */

public abstract class Unit extends Entity {
    protected int hp, ap, mp, dp;
    private Hud hud;

    public Unit(TiledMapActor actor, Player owner) {
        super(actor, owner);
    }

    public void moveTo(TiledMapActor dest) throws MoveUnitException {
        if(getTiledActor().distanceTo(dest) > mp) {
            throw new MoveUnitException("Not enough MP.");
        } else if(!dest.hasBuilding() && dest.getEntities().size() != 0) {
            throw new MoveUnitException("Cell already contains a unit.");
        } else {
            mp -= getTiledActor().distanceTo(dest);
            getTiledActor().getEntities().remove(this);
            setTiledActor(dest);
            getTiledActor().getEntities().add(this);
        }
    }

    abstract void resetMp();

    public int getHp() {
        return hp;
    }

    public int getDp() {
        return dp;
    }

    public int getAp() {
        return ap;
    }

    public int getMp() {
        return mp;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("Unit");
        return res.toString();
    }
}
