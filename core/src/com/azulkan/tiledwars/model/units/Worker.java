package com.azulkan.tiledwars.model.units;

import com.azulkan.tiledwars.model.Constants;
import com.azulkan.tiledwars.model.MissingResourceException;
import com.azulkan.tiledwars.model.Player;
import com.azulkan.tiledwars.controller.TiledMapActor;
import com.azulkan.tiledwars.model.buildings.Archery;
import com.azulkan.tiledwars.model.buildings.Barracks;
import com.azulkan.tiledwars.model.buildings.Tower;
import com.azulkan.tiledwars.model.buildings.Workshop;

/**
 * Created by pauls on 29/11/17.
 */

public class Worker extends Unit {
    private Labor currentLabor;
    public Worker(TiledMapActor actor, Player owner) {
        super(actor, owner);
        this.mp = Constants.WORKER_MOVE_POINTS;
        this.hp = Constants.WORKER_HEALTH_POINTS;
        this.dp = Constants.WORKER_DEFENCE_POINTS;
        this.ap = Constants.WORKER_ATTACK_POINTS;
        this.currentLabor = Labor.NONE;
    }

    public Labor getCurrentLabor() {
        return currentLabor;
    }

    public void setCurrentLabor(Labor labor) {
        this.currentLabor = labor;
    }

    @Override
    public void moveTo(TiledMapActor dest) {
        try {
            super.moveTo(dest);
        } catch(MoveUnitException e) {
            e.printStackTrace();
        }
        switch(dest.getCell().getTile().getId()) {
            case 12: // Wheat
                setCurrentLabor(Labor.FARM); break;

            case 4: case 5: case 6: case 7: // Rock
                setCurrentLabor(Labor.ROCK); break;

            case 14: case 15: case 16: // Wood
                setCurrentLabor(Labor.WOOD); break;

            default:
                setCurrentLabor(Labor.NONE); break;
        }
    }

    public void buildBarracks(TiledMapActor dest) throws com.azulkan.tiledwars.model.MissingResourceException {
        /* Throws exception if player has not enough resources */
        if(this.getOwner().getRock() < Constants.BARRACKS_COST_ROCK ||
                this.getOwner().getWood() < Constants.BARRACKS_COST_WOOD) {
            throw new com.azulkan.tiledwars.model.MissingResourceException("Not enough rock and/or wood to build tent.");
        } else {
           Barracks b = new Barracks(dest, getOwner());
            getOwner().spend("rock", Constants.BARRACKS_COST_ROCK);
            getOwner().spend("wood", Constants.BARRACKS_COST_WOOD);
            getOwner().getBuildings().add(b); // Create the building
            dest.getEntities().add(b);
        }
    }

    public void buildWorkshop(TiledMapActor dest) throws MissingResourceException {
        /* Throws exception if player has not enough resources */
        if(this.getOwner().getRock() < Constants.WORKSHOP_COST_ROCK ||
                this.getOwner().getWood() < Constants.WORKSHOP_COST_WOOD) {
            throw new com.azulkan.tiledwars.model.MissingResourceException("Not enough rock and/or wood to build tent.");
        } else {
            Workshop w = new Workshop(dest, getOwner());
            getOwner().spend("rock", Constants.WORKSHOP_COST_ROCK);
            getOwner().spend("wood", Constants.WORKSHOP_COST_WOOD);
            getOwner().getBuildings().add(w); // Create the building
            dest.getEntities().add(w);
        }
    }

    public void buildArchery(TiledMapActor dest) throws MissingResourceException {
        /* Throws exception if player has not enough resources */
        if(this.getOwner().getRock() < Constants.ARCHERY_COST_ROCK ||
                this.getOwner().getWood() < Constants.ARCHERY_COST_WOOD) {
            throw new com.azulkan.tiledwars.model.MissingResourceException("Not enough rock and/or wood to build archery.");
        } else {
            Archery a = new Archery(dest, getOwner());
            getOwner().spend("rock", Constants.ARCHERY_COST_ROCK);
            getOwner().spend("wood", Constants.ARCHERY_COST_WOOD);
            getOwner().getBuildings().add(a); // Create the building
            dest.getEntities().add(a);
        }
    }

    public void buildTower(TiledMapActor dest) throws MissingResourceException {
        /* Throws exception if player has not enough resources */
        if(this.getOwner().getRock() < Constants.TOWER_COST_ROCK ||
                this.getOwner().getWood() < Constants.TOWER_COST_WOOD) {
            throw new MissingResourceException("Not enough rock and/or wood to build archery.");
        } else {
            Tower t = new Tower(dest, getOwner());
            getOwner().spend("rock", Constants.TOWER_COST_ROCK);
            getOwner().spend("wood", Constants.TOWER_COST_WOOD);
            getOwner().getBuildings().add(t); // Create the building
            dest.getEntities().add(t);
        }
    }

    public void buildFarm(TiledMapActor dest) throws com.azulkan.tiledwars.model.MissingResourceException, MoveUnitException {
        /* Throw exception if player has not enough resource */
        if(this.getOwner().getWheat() < Constants.FARM_COST_WHEAT ||
                this.getOwner().getWood() < Constants.FARM_COST_WOOD) {
            throw new com.azulkan.tiledwars.model.MissingResourceException("Not enough wheat and/or wood to build farm.");
        } else if(this.getTiledActor().distanceTo(dest) > getMp()) {
            throw new MoveUnitException("Not enough MP to build here.");
        } else {
            getOwner().spend("wheat", Constants.FARM_COST_WHEAT);
            getOwner().spend("wood", Constants.FARM_COST_WOOD);
            moveTo(dest); // Move the worker to destination
            this.setCurrentLabor(Labor.FARM); // Create the building
        }
    }

    @Override
    public void resetMp() {
        this.mp = Constants.WORKER_MOVE_POINTS;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("Worker");
        return res.toString();
    }
}
