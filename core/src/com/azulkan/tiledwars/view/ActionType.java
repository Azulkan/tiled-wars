package com.azulkan.tiledwars.view;

/**
 * Created by pauls on 01/12/17.
 */

public enum ActionType {
    SELECT_TILE,
    MENU,
    MOVE_UNIT,
    BUILD_BARRACKS,
    BUILD_ARCHERY,
    BUILD_WORKSHOP,
    BUILD_TOWER,
    BUILD_FARM
}
