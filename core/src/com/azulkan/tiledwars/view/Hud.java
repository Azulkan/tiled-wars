package com.azulkan.tiledwars.view;

import com.azulkan.tiledwars.model.Constants;
import com.azulkan.tiledwars.model.Game;
import com.azulkan.tiledwars.model.Player;
import com.azulkan.tiledwars.model.Entity;
import com.azulkan.tiledwars.model.MissingResourceException;
import com.azulkan.tiledwars.model.units.Unit;
import com.azulkan.tiledwars.controller.MissingUnitException;
import com.azulkan.tiledwars.controller.TiledMapActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;


/**
 * Created by Paul on 28/11/2017.
 */

public class Hud implements Disposable {
    private Game game;
    private Player player;
    public Stage stage;
    public Viewport viewport;
    private Skin skin;
    private BitmapFont font;
    private ActionType action;

    private Entity selection;

    /* Scene2D widgets */
    private Table gameInfoTable, selectionTable;
    private Label timeLbl, playerLbl;
    private Label wheatLbl, logLbl, rockLbl, handLbl; // Labels in the resource menu
    private Label swordLbl, maceLbl, bowLbl, horseLbl, cannonLbl, catapultLbl; // Labels in the resource menu
    private Label handLblMenu, crownLblMenu, swordLblMenu, maceLblMenu; // Labels in the building menu

    public Hud(SpriteBatch sb, Game g) {
        game = g;
        player = g.getPlayers().get(0);
        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), new OrthographicCamera());
        stage = new Stage(viewport, sb);
        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/BEBAS.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 50;
        font = generator.generateFont(parameter);

        action = ActionType.SELECT_TILE;

        gameInfoTable = new Table();
        gameInfoTable.left().top();
        gameInfoTable.setFillParent(true);
        gameInfoTable.pad(15);

        makeResourceTable();
        makeArmyTable();
        makeRoundTable();

        selectionTable = new Table();
        selectionTable.left().top();
        selectionTable.setFillParent(true);
        selectionTable.pad(10).padTop(300);

        //add table to the stage
        stage.addActor(gameInfoTable);
        stage.addActor(selectionTable);

    }

    public void makeResourceTable() {
        Image handImg = new Image(new Texture(Gdx.files.internal("hand.png")));
        Image logImg = new Image(new Texture(Gdx.files.internal("res-log.png")));
        Image wheatImg = new Image(new Texture(Gdx.files.internal("res-wheat.png")));
        Image rockImg = new Image(new Texture(Gdx.files.internal("res-rock.png")));
        handLbl = new Label("3", new LabelStyle(font, Color.WHITE));
        logLbl = new Label("0", new LabelStyle(font, Color.WHITE));
        wheatLbl = new Label("100", new LabelStyle(font, Color.WHITE));
        rockLbl = new Label("0", new LabelStyle(font, Color.WHITE));

        Table table = new Table();

        table.add(wheatImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(wheatLbl).pad(0, 0, 0, 20);
        //table.row();
        table.add(logImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(logLbl).pad(0, 0, 0, 0);

        table.row();

        table.add(rockImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(rockLbl).pad(0, 0, 0, 20);
        //table.row();
        table.add(handImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(handLbl).pad(0, 0, 0, 0);

        table.setBackground(new NinePatchDrawable(getNinePatch("ninepatch.9.png", 33)));

        gameInfoTable.add(table);
    }

    public void makeArmyTable() {
        Image swordImg = new Image(new Texture(Gdx.files.internal("sword.png")));
        Image maceImg = new Image(new Texture(Gdx.files.internal("mace.png")));
        Image bowImg = new Image(new Texture(Gdx.files.internal("bow.png")));
        Image horseImg = new Image(new Texture(Gdx.files.internal("horse.png")));
        Image cannonImg = new Image(new Texture(Gdx.files.internal("cannon.png")));
        Image catapultImg = new Image(new Texture(Gdx.files.internal("catapult.png")));
        swordLbl = new Label("0", new LabelStyle(font, Color.WHITE));
        maceLbl = new Label("0", new LabelStyle(font, Color.WHITE));
        bowLbl = new Label("0", new LabelStyle(font, Color.WHITE));
        horseLbl = new Label("0", new LabelStyle(font, Color.WHITE));
        cannonLbl = new Label("0", new LabelStyle(font, Color.WHITE));
        catapultLbl = new Label("0", new LabelStyle(font, Color.WHITE));

        Table table = new Table();

        table.add(swordImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(swordLbl).pad(0, 0, 0, 20);
        //resTable.row();
        table.add(maceImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(maceLbl).pad(0, 0, 0, 20);
        //resTable.row();
        table.add(bowImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(bowLbl).pad(0, 0, 0, 0);
        table.row();
        table.add(horseImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(horseLbl).pad(0, 0, 0, 20);
        //resTable.row();
        table.add(cannonImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(cannonLbl).pad(0, 0, 0, 20);
        //resTable.row();
        table.add(catapultImg).size(80, 80).pad(0, 0, 0, 10);
        table.add(catapultLbl).pad(0, 0, 0, 0);
        //resTable.row();

        table.setBackground(new NinePatchDrawable(getNinePatch("ninepatch.9.png", 33)));

        gameInfoTable.add(table).padLeft(10);
    }

    public void makeRoundTable() {
        timeLbl = new Label(game.getRound().getStrTime(), new LabelStyle(font, Color.WHITE));
        playerLbl = new Label("Player " + game.getRound().getPlayer().getId(), new LabelStyle(font, Color.WHITE));
        Table table = new Table();
        table.add(playerLbl).padBottom(10);
        table.row();
        table.add(timeLbl);
        table.setBackground(new NinePatchDrawable(getNinePatch("ninepatch.9.png", 33)));
        gameInfoTable.add(table).padLeft(10).expandX().align(Align.top).align(Align.right);//.expandX().align(Align.right);
    }

    public void makeWorkerTable(final TiledMapActor actor) {
        if(action == ActionType.SELECT_TILE) {
            action = ActionType.MOVE_UNIT;
            /* Layout */
            Image barrackImg = new Image(new Texture(Gdx.files.internal("tent.png")));
            Image archeryImg = new Image(new Texture(Gdx.files.internal("tent2.png")));
            Image towerImg = new Image(new Texture(Gdx.files.internal("tower.png")));
            Image farmImg = new Image(new Texture(Gdx.files.internal("tiles/farm.png")));
            Image workshopImg = new Image(new Texture(Gdx.files.internal("workshop.png")));

            Table table = new Table();

            /* Infos about the worker */
            Table workerInfoTable = new Table();
            try {
                //workerInfoTable.add(new Label("---- Worker ----", new LabelStyle(font, Color.WHITE))).colspan(7).padBottom(10).row();
                workerInfoTable.add(new Image(new Texture(Gdx.files.internal("blood.png")))).size(50, 50).pad(10, 0, 10, 3);
                workerInfoTable.add(new Label(String.valueOf(actor.selectWorker().getHp()), new LabelStyle(font, Color.WHITE))).padRight(20);
                workerInfoTable.add(new Image(new Texture(Gdx.files.internal("shield.png")))).size(50, 50).pad(10, 0, 10, 3);
                workerInfoTable.add(new Label(String.valueOf(actor.selectWorker().getDp()), new LabelStyle(font, Color.WHITE))).padRight(20);
                workerInfoTable.add(new Image(new Texture(Gdx.files.internal("explosion.png")))).size(50, 50).pad(10, 0, 10, 3);
                workerInfoTable.add(new Label(String.valueOf(actor.selectWorker().getAp()), new LabelStyle(font, Color.WHITE)));
                workerInfoTable.padBottom(20);
                table.add(workerInfoTable).colspan(7).row();
            } catch(MissingUnitException e) {
                e.printStackTrace();
            }

            table.add(new Label("----- Build -----", new LabelStyle(font, Color.WHITE))).colspan(7).row();

            /* Create farm row */
            table.add(farmImg).size(80, 80).pad(10, 0, 10, 20);
            table.add(new Label("(", new LabelStyle(font, Color.WHITE)));
            table.add(new Image(new Texture(Gdx.files.internal("res-wheat.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.FARM_COST_WHEAT), new LabelStyle(font, Color.WHITE))).pad(10, 0, 10, 20);;
            table.add(new Image(new Texture(Gdx.files.internal("res-log.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.FARM_COST_WOOD), new LabelStyle(font, Color.WHITE)));
            table.add(new Label(")", new LabelStyle(font, Color.WHITE))).row();
            farmImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if(player.getWheat() >= Constants.FARM_COST_WHEAT &&
                            player.getWood() >= Constants.FARM_COST_WOOD) {
                        System.out.println("Creating farm.");
                        clear(); // Hide worker's menu
                        setActionType(ActionType.BUILD_FARM);
                    }
                }
            });

            /* Create barracks row */
            table.add(barrackImg).size(80, 80).pad(10, 0, 10, 20);
            table.add(new Label("(", new LabelStyle(font, Color.WHITE)));
            table.add(new Image(new Texture(Gdx.files.internal("res-log.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.BARRACKS_COST_WOOD), new LabelStyle(font, Color.WHITE))).pad(10, 0, 10, 20);;
            table.add(new Image(new Texture(Gdx.files.internal("res-rock.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.BARRACKS_COST_ROCK), new LabelStyle(font, Color.WHITE)));
            table.add(new Label(")", new LabelStyle(font, Color.WHITE))).row();
            barrackImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    System.out.println("Creating Barracks.");
                    clear(); // Hide worker's menu
                    setActionType(ActionType.BUILD_BARRACKS);
                }
            });

            /* Create archery row */
            table.add(archeryImg).size(80, 80).pad(10, 0, 10, 20);
            table.add(new Label("(", new LabelStyle(font, Color.WHITE)));
            table.add(new Image(new Texture(Gdx.files.internal("res-log.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.ARCHERY_COST_WOOD), new LabelStyle(font, Color.WHITE))).pad(10, 0, 10, 20);;
            table.add(new Image(new Texture(Gdx.files.internal("res-rock.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.ARCHERY_COST_ROCK), new LabelStyle(font, Color.WHITE)));
            table.add(new Label(")", new LabelStyle(font, Color.WHITE))).row();
            archeryImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    System.out.println("Creating archery.");
                    clear(); // Hide worker's menu
                    setActionType(ActionType.BUILD_ARCHERY);
                }
            });

            /* Create workshop row */
            table.add(workshopImg).size(80, 80).pad(10, 0, 10, 20);
            table.add(new Label("(", new LabelStyle(font, Color.WHITE)));
            table.add(new Image(new Texture(Gdx.files.internal("res-log.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.WORKSHOP_COST_WOOD), new LabelStyle(font, Color.WHITE))).pad(10, 0, 10, 20);;
            table.add(new Image(new Texture(Gdx.files.internal("res-rock.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.WORKSHOP_COST_ROCK), new LabelStyle(font, Color.WHITE)));
            table.add(new Label(")", new LabelStyle(font, Color.WHITE))).row();
            workshopImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    System.out.println("Creating workshop.");
                    clear(); // Hide worker's menu
                    setActionType(ActionType.BUILD_WORKSHOP);
                }
            });

            /* Create tower row */
            table.add(towerImg).size(80, 80).pad(10, 0, 10, 20);
            table.add(new Label("(", new LabelStyle(font, Color.WHITE)));
            table.add(new Image(new Texture(Gdx.files.internal("res-log.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.TOWER_COST_WOOD), new LabelStyle(font, Color.WHITE))).pad(10, 0, 10, 20);;
            table.add(new Image(new Texture(Gdx.files.internal("res-rock.png")))).size(50, 50).pad(10, 0, 10, 3);
            table.add(new Label(String.valueOf(Constants.TOWER_COST_ROCK), new LabelStyle(font, Color.WHITE)));
            table.add(new Label(")", new LabelStyle(font, Color.WHITE))).row();
            towerImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    System.out.println("Creating tower.");
                    clear(); // Hide worker's menu
                    setActionType(ActionType.BUILD_TOWER);
                }
            });

            table.setBackground(new NinePatchDrawable(getNinePatch("ninepatch.9.png", 33)));
            selectionTable.add(table).expandX().align(Align.left);
        }
    }

    public void makeFightTable(final TiledMapActor actor) {

    }

    public void makeCastleTable(final TiledMapActor actor) {
        if(action == ActionType.SELECT_TILE) {
            action = ActionType.MENU;
            /* Layout */
            Image handImg = new Image(new Texture(Gdx.files.internal("hand.png")));
            Image crownImg = new Image(new Texture(Gdx.files.internal("crown.png")));
            Image plusHandImg = new Image(new Texture(Gdx.files.internal("cross.png")));
            handLblMenu = new Label(String.valueOf(player.getWorkersCount(actor)) + "/" + String.valueOf(player.getWorkersCount()), new LabelStyle(font, Color.WHITE));
            crownLblMenu = new Label(String.valueOf(player.getKingsCount(actor)) + "/" + String.valueOf(player.getKingsCount()), new LabelStyle(font, Color.WHITE));

            Table table = new Table();
            table.add(crownImg).size(80, 80).pad(10, 0, 10, 30);
            table.add(crownLblMenu).pad(10, 0, 10, 0);
            table.row();
            table.add(handImg).size(80, 80).pad(10, 0, 10, 30);
            table.add(handLblMenu).pad(10, 0, 10, 30);
            table.add(plusHandImg).size(80, 80).pad(10, 0, 10, 0);
            table.setBackground(new NinePatchDrawable(getNinePatch("ninepatch.9.png", 33)));

            //rootTable.row();
            selectionTable.add(table).expandX().align(Align.center);

            /* EventListeners */
            crownImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    try {
                        clear();
                        action = ActionType.MOVE_UNIT;
                        selection = actor.selectKing();
                    } catch(MissingUnitException e) {
                        e.printStackTrace();
                    }
                }
            });

            plusHandImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    System.out.println("Creating worker");
                    try {
                        game.getPlayers().get(0).createWorker(actor);
                        handLblMenu.setText(String.valueOf(player.getWorkersCount(actor)) + "/" + String.valueOf(player.getWorkersCount()));
                    } catch(MissingResourceException e) {
                        e.printStackTrace();
                    }
                }
            });

            handImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    try {
                        clear();
                        action = ActionType.MOVE_UNIT;
                        selection = actor.selectWorker();
                        /* Display range overlay */

                    } catch(MissingUnitException e) {
                        e.printStackTrace();
                    }
                }
            });
        };
    }

    public void makeBarracksTable(final TiledMapActor actor) {
        if(action == ActionType.SELECT_TILE) {
            action = ActionType.MENU;
            /* Layout */
            Image swordImg = new Image(new Texture(Gdx.files.internal("sword.png")));
            Image maceImg = new Image(new Texture(Gdx.files.internal("mace.png")));
            Image plusSwordImg = new Image(new Texture(Gdx.files.internal("cross.png")));
            Image plusMaceImg = new Image(new Texture(Gdx.files.internal("cross.png")));
            swordLblMenu = new Label(String.valueOf(player.getSwordmenCount(actor)) + "/" + String.valueOf(player.getSwordmenCount()), new LabelStyle(font, Color.WHITE));
            maceLblMenu = new Label(String.valueOf(player.getMacemenCount(actor)) + "/" + String.valueOf(player.getMacemenCount()), new LabelStyle(font, Color.WHITE));

            Table table = new Table();
            table.add(swordImg).size(80, 80).pad(10, 0, 10, 20);
            table.add(swordLblMenu).pad(10, 0, 10, 20);
            table.add(plusSwordImg).size(80, 80).pad(10, 0, 10, 0);
            table.row();
            table.add(maceImg).size(80, 80).pad(10, 0, 10, 20);
            table.add(maceLblMenu).pad(10, 0, 10, 20);
            table.add(plusMaceImg).size(80, 80).pad(10, 0, 10, 0);
            table.setBackground(new NinePatchDrawable(getNinePatch("ninepatch.9.png", 33)));

            //rootTable.row();
            selectionTable.add(table).expandX().align(Align.center);

            /* EventListeners */
            swordImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    try {
                        clear();
                        action = ActionType.MOVE_UNIT;
                        selection = actor.selectSwordman();
                    } catch(MissingUnitException e) {
                        e.printStackTrace();
                    }
                }
            });

            plusSwordImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    System.out.println("Creating swordman");
                    try {
                        game.getPlayers().get(0).createSwordman(actor);
                        swordLblMenu.setText(String.valueOf(player.getSwordmenCount(actor)) + "/" + String.valueOf(player.getSwordmenCount()));
                    } catch(MissingResourceException e) {
                        e.printStackTrace();
                    }
                }
            });

            maceImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    try {
                        clear();
                        action = ActionType.MOVE_UNIT;
                        selection = actor.selectMaceman();
                        /* Display range overlay */

                    } catch(MissingUnitException e) {
                        e.printStackTrace();
                    }
                }
            });

            plusMaceImg.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    System.out.println("Creating maceman");
                    try {
                        game.getPlayers().get(0).createMaceman(actor);
                        maceLblMenu.setText(String.valueOf(player.getMacemenCount(actor)) + "/" + String.valueOf(player.getMacemenCount()));
                    } catch(MissingResourceException e) {
                        e.printStackTrace();
                    }
                }
            });
        };
    }

    public void update() {
        playerLbl.setText("Player " + game.getRound().getPlayer().getId());
        timeLbl.setText(game.getRound().getStrTime());
        wheatLbl.setText(String.valueOf(game.getRound().getPlayer().getWheat()));
        logLbl.setText(String.valueOf(game.getRound().getPlayer().getWood()));
        rockLbl.setText(String.valueOf(game.getRound().getPlayer().getRock()));
        handLbl.setText(String.valueOf(game.getRound().getPlayer().getWorkersCount()));
        swordLbl.setText(String.valueOf(game.getRound().getPlayer().getSwordmenCount()));
        maceLbl.setText(String.valueOf(game.getRound().getPlayer().getMacemenCount()));
        /*bowLbl.setText(String.valueOf(game.getRound().getPlayer().getArchersCount()));
        swordLbl.setText(String.valueOf(game.getRound().getPlayer().getHorsesCount()));
        swordLbl.setText(String.valueOf(game.getRound().getPlayer().getCannonsCount()));
        swordLbl.setText(String.valueOf(game.getRound().getPlayer().getCatapultsCount()));*/

        /* Update overlay sprites */
        updateOverlay();
    }

    public void clear() {
        selectionTable.clearChildren();
    }

    public void updateOverlay() {
        /* Update overlay sprites */
        TiledMapTileLayer overlayLayer = (TiledMapTileLayer) game.getMap().getLayers().get("overlay");
        Entity selection = getSelection();
        TiledMapTile tile = game.getMap().getTileSets().getTileSet("overlay").getTile(46);
        for(int x = 0; x < overlayLayer.getWidth(); x++) {
            for(int y = 0; y < overlayLayer.getHeight(); y++) {
                overlayLayer.setCell(x, y, null); // Clear all cells
                if(selection instanceof Unit) {
                    if(getActionType() == ActionType.MOVE_UNIT) {
                        if(((Unit)selection).getMp() >= selection.getTiledActor().distanceTo(x, y)) { // If the cell is at range
                            overlayLayer.setCell(x, y, new TiledMapTileLayer.Cell());
                            overlayLayer.getCell(x, y).setTile(tile);
                        }
                    } else if(getActionType() == ActionType.BUILD_BARRACKS || getActionType() == ActionType.BUILD_ARCHERY || getActionType() == ActionType.BUILD_WORKSHOP ||  getActionType() == ActionType.BUILD_TOWER) {
                        if(selection.getTiledActor().distanceTo(x, y) == 1) { // If the cell is adjacent to the worker
                            overlayLayer.setCell(x, y, new TiledMapTileLayer.Cell());
                            overlayLayer.getCell(x, y).setTile(tile);
                        }
                    } else if(getActionType() == ActionType.BUILD_FARM) {
                        if(selection.getTiledActor().distanceTo(x, y) <= 1) { // If the cell is adjacent or the same as the worker's
                            overlayLayer.setCell(x, y, new TiledMapTileLayer.Cell());
                            overlayLayer.getCell(x, y).setTile(tile);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private NinePatch getNinePatch(String fname, int halfSize) {
        // Get the image
        final Texture t = new Texture(Gdx.files.internal(fname));

        // create a new texture region, otherwise black pixels will show up too, we are simply cropping the image
        // last 4 numbers respresent the length of how much each corner can draw,
        // for example if your image is 50px and you set the numbers 50, your whole image will be drawn in each corner
        // so what number should be good?, well a little less than half would be nice
        return new NinePatch(new TextureRegion(t, 1, 1 , t.getWidth() - 2, t.getHeight() - 2), halfSize, halfSize, halfSize, halfSize);
    }

    public Entity getSelection() {
        return selection;
    }

    public ActionType getActionType() {
        return action;
    }

    public void setActionType(ActionType action) {
        this.action = action;
    }

    public void setSelection(Entity selection) {
        this.selection = selection;
    }

    public Game getGame() {
        return this.game;
    }
}
